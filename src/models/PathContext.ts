import Post from './Post'

interface PathContext {
  tags?: string[]
  tagName?: string
  posts?: Post[]
  next: any
  prev: any
}

export default PathContext
