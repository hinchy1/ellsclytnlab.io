interface Frontmatter {
  date: string
  title: string
  tags: string[]
}

export default Frontmatter
