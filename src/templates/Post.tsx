import React, { Fragment } from 'react'
import Helmet from 'react-helmet'
import { Link, graphql } from 'gatsby'
import kebabCase from 'lodash/kebabCase'
import {
  Layout,
  Header,
  Subline,
  SEO,
  SectionTitle,
  Content
} from '../components'
import config from '../../config/SiteConfig'
import '../utils/prismjs-theme.css'
import PathContext from '../models/PathContext'
import Post from '../models/Post'

interface Props {
  data: {
    markdownRemark: Post
  }
  pathContext: PathContext
}

export default class PostPage extends React.PureComponent<Props> {
  public render () {
    const post = this.props.data.markdownRemark
    return (
      <Layout>
        {post ? (
          <article>
            <SEO postPath={post.fields.slug} postNode={post} postSEO />
            <Helmet title={`${post.frontmatter.title} | ${config.siteTitle}`} />
            <Header>
              <SectionTitle>{post.frontmatter.title}</SectionTitle>
              <Subline light={true}>
                {post.frontmatter.date} &mdash; {post.timeToRead} min read
              </Subline>
            </Header>
            <Content>
              <div dangerouslySetInnerHTML={{ __html: post.html }} />
              {post.frontmatter.tags ? (
                <Subline>
                  Tags: &#160;
                  {post.frontmatter.tags.map((tag, i) => (
                    <Fragment key={`tag-${i}`}>
                      <Link to={`/tags/${kebabCase(tag)}`}>
                        <strong>{tag}</strong>
                      </Link>
                      {i < post.frontmatter.tags.length - 1 ? `, ` : ``}
                    </Fragment>
                  ))}
                </Subline>
              ) : null}
            </Content>
          </article>
        ) : null}
      </Layout>
    )
  }
}

export const postQuery = graphql`
  query($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        title
        date(formatString: "DD MMM YYYY")
        tags
      }
      timeToRead
    }
  }
`
