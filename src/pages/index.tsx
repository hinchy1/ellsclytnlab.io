import React from 'react'
import { Link, graphql } from 'gatsby'
import styled from 'styled-components'
import { Layout, Wrapper, Article } from '../components'
import PageProps from '../models/PageProps'
import Helmet from 'react-helmet'
import config from '../../config/SiteConfig'
import { media } from '../utils/media'

const Homepage = styled.main`
  display: flex;
  height: 100vh;
  flex-direction: row;
  @media ${media.tablet} {
    height: 100%;
    flex-direction: column;
  }
  @media ${media.phone} {
    height: 100%;
    flex-direction: column;
  }
`

const GridRow = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  background: ${({ theme }) => theme.colors.base[0]};
  background-size: cover;
  padding: 2rem 4rem;
  color: ${({ theme }) => theme.colors.base[3]};
  h1 {
    color: ${({ theme }) => theme.colors.base[3]};
  }
  @media ${media.tablet} {
    padding: 3rem 3rem;
  }
  @media ${media.phone} {
    padding: 2rem 1.5rem;
  }
`

const HomepageContent = styled.div<{
  center?: boolean
}>`
  max-width: 30rem;
  text-align: ${props => (props.center ? 'center' : 'left')};
`

export default class IndexPage extends React.Component<PageProps> {
  public render () {
    const { data } = this.props
    const { edges, totalCount } = data.allMarkdownRemark
    return (
      <Layout navVisible={false}>
        <Wrapper fullWidth={true}>
          <Helmet title={config.siteTitle} />
          <Homepage>
            <GridRow>
              <HomepageContent center>
                <img src={config.siteLogo} alt='' />
                <h1>Ellis Clayton</h1>
              </HomepageContent>
            </GridRow>
            <GridRow>
              <HomepageContent>
                <p>
                  Hey, I'm Ellis, a full-stack TypeScript, JavaScript (and
                  sometimes other script) developer in Melbourne. If you&rsquo;d
                  like to contact me, you may reach me on{' '}
                  <a
                    href='https://au.linkedin.com/in/ellsclytn'
                    target='_blank'
                    rel='noopener'
                  >
                    LinkedIn
                  </a>
                  . Feel free to check out what I&rsquo;m up to on{' '}
                  <a
                    href='https://github.com/ellsclytn'
                    target='_blank'
                    rel='noopener'
                  >
                    GitHub
                  </a>
                  ,{' '}
                  <a
                    href='https://gitlab.com/ellsclytn'
                    target='_blank'
                    rel='noopener'
                  >
                    GitLab
                  </a>
                  , or in my poorly maintained <Link to='/blog'>blog</Link>.
                </p>
                <hr />
                {edges.map(post => (
                  <Article
                    title={post.node.frontmatter.title}
                    date={post.node.frontmatter.date}
                    excerpt={post.node.excerpt}
                    timeToRead={post.node.timeToRead}
                    slug={post.node.fields.slug}
                    key={post.node.fields.slug}
                  />
                ))}
              </HomepageContent>
            </GridRow>
          </Homepage>
        </Wrapper>
      </Layout>
    )
  }
}
export const IndexQuery = graphql`
  query {
    allMarkdownRemark(
      sort: { fields: [frontmatter___date], order: DESC }
      limit: 1
    ) {
      totalCount
      edges {
        node {
          fields {
            slug
          }
          frontmatter {
            title
            date(formatString: "DD/MM/YYYY")
          }
          timeToRead
        }
      }
    }
  }
`
