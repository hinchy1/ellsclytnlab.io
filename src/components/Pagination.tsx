import React, { memo } from 'react'
import { Link } from 'gatsby'
import styled from 'styled-components'

const PaginationContainer = styled.div`
  a {
    text-decoration: none;
  }
`

const Separator = styled.span`
  &::before {
    content: ' | ';
  }
`

interface Props {
  currentPage: number
  totalPages: number
  url: string
}

export const Pagination: React.FC<Props> = memo(
  ({ currentPage, totalPages, url }) => {
    const isFirst = currentPage === 1
    const isLast = currentPage === totalPages
    const prevPage =
      currentPage - 1 === 1
        ? `/${url}/`
        : `/${url}/${(currentPage - 1).toString()}`
    const nextPage = `/${url}/${(currentPage + 1).toString()}`

    if (totalPages < 1) return null

    return (
      <PaginationContainer>
        {!isFirst && (
          <Link to={prevPage} rel='prev'>
            ← Prev
          </Link>
        )}
        {!isFirst && !isLast && <Separator />}
        {!isLast && (
          <Link to={nextPage} rel='next'>
            Next →
          </Link>
        )}
      </PaginationContainer>
    )
  }
)
