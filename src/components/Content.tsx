import styled from 'styled-components'
import { media } from '../utils/media'

export const Content = styled.div`
  margin: 0 auto;
  padding: 2rem 1rem;
  max-width: 76ch;

  @media ${media.tablet} {
    padding: 1.5rem;
  }

  @media ${media.phone} {
    padding: 1rem;
  }

  .anchor {
    fill: ${({ theme }) => theme.colors.base[2]};
  }
`
