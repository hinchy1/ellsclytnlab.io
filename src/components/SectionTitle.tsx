import styled from 'styled-components'

export const SectionTitle = styled.h1`
  font-size: ${({ theme }) => theme.fontSize.big};
  text-align: center;
  color: ${({ theme }) => theme.colors.white};
  position: relative;
  padding: 2rem 0 0;
  margin-bottom: 2rem;
`
