import React, { memo } from 'react'
import { Link } from 'gatsby'
import styled from 'styled-components'
import { media } from '../utils/media'

const NavContainer = styled.nav`
  background: ${({ theme }) => theme.colors.base[1]};
  text-align: center;
  padding: 4rem 0 1rem;

  @media ${media.phone} {
    padding: 1rem 0;
  }
`

const NavList = styled.ul`
  list-style-type: none;
  margin: 0;

  li {
    display: inline;
  }

  li + li::before {
    content: ' | ';
  }

  a {
    text-decoration: none;
  }
`

export const Nav: React.FC = memo(() => (
  <NavContainer>
    <NavList>
      <li>
        <Link to='/'>Home</Link>
      </li>
      <li>
        <Link to='/blog'>Blog</Link>
      </li>
    </NavList>
  </NavContainer>
))
