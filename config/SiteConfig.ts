export default {
  /** Prefix for all links. If you deploy your site to example.com/portfolio your pathPrefix should be "portfolio" */
  pathPrefix: '/',

  /** Navigation and Site Title */
  siteTitle: 'Ellis Clayton',
  /** Domain of your site. No trailing slash! */
  siteUrl: 'https://ellis.codes',
  /** Language Tag on <html> element */
  siteLanguage: 'en',
  /** Your image for favicons. You can find it in the /src folder */
  favicon: 'src/favicon.png',
  /** Your site description */
  siteDescription: 'Personal blog of Ellis Clayton',
  /** Author for schemaORGJSONLD */
  author: 'Ellis Clayton',
  /** Image for schemaORGJSONLD */
  siteLogo: '/assets/logo.svg',

  /** Twitter Username - Optional */
  userTwitter: '@ellsclytn',
  /** Facebook Site Name - Optional */
  ogSiteName: 'Ellis Clayton',
  /** Facebook Language */
  ogLanguage: 'en_AU',

  /** Manifest and Progress color
   * See: https://developers.google.com/web/fundamentals/web-app-manifest/
   */
  themeColor: '#2B3A42',
  backgroundColor: '#2B3A42',

  /** Settings for typography.ts */
  headerFontFamily: 'Zilla Slab',
  bodyFontFamily: 'azo-sans-web',
  baseFontSize: '18px',

  POST_PER_PAGE: 4
}
