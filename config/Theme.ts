const colors = {
  /** Color for buttons or links */
  primary: '#FF530D',
  /** Background color */
  bg: '#2B3A42',
  white: '#EFEFEF',
  base: ['#2B3A42', '#3F5765', '#BDD4DE', '#EFEFEF', '#FF530D'],
  grey: {
    dark: 'rgba(0, 0, 0, 0.9)',
    default: 'rgba(0, 0, 0, 0.7)',
    light: 'rgba(0, 0, 0, 0.5)',
    ultraLight: 'rgba(0, 0, 0, 0.25)'
  }
}

const transitions = {
  normal: '0.1s'
}

const fontSize = {
  small: '0.9rem',
  big: '2.9rem'
}

export default {
  colors,
  transitions,
  fontSize
}
